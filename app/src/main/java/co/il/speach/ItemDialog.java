package co.il.speach;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.TextView;

import java.util.Objects;

import co.il.speach.server.testWiki.ResponseWiki;

class ItemDialog {

    private ResponseWiki itemDetailes;
    private ItemDialogListener mListener;


    ItemDialog(ResponseWiki itemDetailes) {
        this.itemDetailes = itemDetailes;
    }


    void showDialog(Context context, ItemDialogListener listener) {

        if (context != null) {

            mListener = listener;
            Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_item);
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
            dialog.show();

            TextView mItemText = dialog.findViewById(R.id.DI_item_text_TV);
            mItemText.setText(itemDetailes.getExtract());

            TextView mItemLink = dialog.findViewById(R.id.DI_link_TV);

            SpannableStringBuilder ssb = new SpannableStringBuilder();
            ssb.append(mItemLink.getText());
            ssb.setSpan(new URLSpan("#"), 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            mItemLink.setText(ssb, TextView.BufferType.SPANNABLE);

            mItemLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   mListener.onLinkWikiClicked(itemDetailes);
                }
            });


        }


    }


    interface ItemDialogListener {


        void onLinkWikiClicked(ResponseWiki itemDetailes);
    }
}
