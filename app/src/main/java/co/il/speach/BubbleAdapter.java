package co.il.speach;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import co.il.speach.server.RequestManager;
import co.il.speach.server.testWiki.ResponseWiki;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;



public class BubbleAdapter extends RecyclerView.Adapter<BubbleAdapter.ViewHolder>{


    private final Context mContext;
    private final BubbleAdapterListener mListener;
    private List<String> mData;
    private int lastPosition = -1;


    BubbleAdapter(Context context, List<String> data, BubbleAdapterListener listener) {

        this.mData = data;
        this.mContext = context;
        this.mListener = listener;

    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bubble_item, parent, false);
        return new ViewHolder(view);
    }







    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.updateItem(position);

//        setAnimation(holder.itemView, position);

        holder.mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.mProgressBar.setVisibility(View.VISIBLE);
                RequestManager.getWikipedia(mData.get(position)).subscribe(new Observer<ResponseWiki>() {


                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseWiki value) {


//                        String results = value.getQuery().getPages().get(value.getQuery().getPages().keySet().toString().substring(1, value.getQuery().getPages().keySet().toString().length() - 1)).getContent();
                        mListener.onItemWikiFind(value);
                        holder.mProgressBar.setVisibility(View.GONE);

//                        holder.mTextView.setText(results);


                    }


                    @Override
                    public void onError(Throwable e) {


                    }


                    @Override
                    public void onComplete() {

                    }

                });

            }
        });
    }





    private void setAnimation(View itemView, int position) {

            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.sample_animation);
            itemView.startAnimation(animation);
            lastPosition = position;

    }





    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private final ProgressBar mProgressBar;
        TextView mTextView;

        ViewHolder(View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.RR_keywoed_TV);
            mProgressBar = itemView.findViewById(R.id.RR_progress_bar_PB);
        }


        void updateItem(int position) {

            String keyword = mData.get(position);
            mTextView.setText(keyword);



        }
    }


    public interface BubbleAdapterListener{


        void onItemWikiFind(ResponseWiki results);
    }

}
