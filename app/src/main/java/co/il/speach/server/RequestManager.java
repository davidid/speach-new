package co.il.speach.server;

import co.il.speach.server.testWiki.ResponseWiki;
import co.il.speach.server.wiki_model.Result;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class RequestManager {

    private static final String TOKEN = "token ";

    public static Observable<ResponseWiki> getWikipedia(String wordToWiki) {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        return service.getWikipedia(wordToWiki)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(10, TimeUnit.SECONDS);
    }








}
