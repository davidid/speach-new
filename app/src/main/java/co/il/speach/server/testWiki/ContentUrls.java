package co.il.speach.server.testWiki;

import com.google.gson.annotations.SerializedName;

public class ContentUrls{

	@SerializedName("desktop")
	private Desktop desktop;

	@SerializedName("mobile")
	private Mobile mobile;

	public void setDesktop(Desktop desktop){
		this.desktop = desktop;
	}

	public Desktop getDesktop(){
		return desktop;
	}

	public void setMobile(Mobile mobile){
		this.mobile = mobile;
	}

	public Mobile getMobile(){
		return mobile;
	}
}