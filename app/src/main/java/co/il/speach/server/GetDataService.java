package co.il.speach.server;


import co.il.speach.server.testWiki.ResponseWiki;
import co.il.speach.server.wiki_model.Result;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetDataService {

//    @GET("api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&")
    @GET("{wordToWiki}")
    Observable<ResponseWiki> getWikipedia(@Path("wordToWiki") String wordToWiki);



}
