package co.il.speach.server.wiki_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Page {

    @SerializedName("pageid")
    private long id;
    @SerializedName("title")
    private String title;
    @SerializedName("extract")
    private String content;


    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}