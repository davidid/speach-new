package co.il.speach.server.wiki_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Result {
    @SerializedName("batchcomplete")
    private String result;
    @SerializedName("query")
    private Query query;


    public String getResult() {
        return result;
    }

    public Query getQuery() {
        return query;
    }
}