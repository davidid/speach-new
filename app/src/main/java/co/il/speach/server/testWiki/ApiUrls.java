package co.il.speach.server.testWiki;

import com.google.gson.annotations.SerializedName;

public class ApiUrls{

	@SerializedName("summary")
	private String summary;

	@SerializedName("edit_html")
	private String editHtml;

	@SerializedName("metadata")
	private String metadata;

	@SerializedName("references")
	private String references;

	@SerializedName("media")
	private String media;

	@SerializedName("talk_page_html")
	private String talkPageHtml;

	public void setSummary(String summary){
		this.summary = summary;
	}

	public String getSummary(){
		return summary;
	}

	public void setEditHtml(String editHtml){
		this.editHtml = editHtml;
	}

	public String getEditHtml(){
		return editHtml;
	}

	public void setMetadata(String metadata){
		this.metadata = metadata;
	}

	public String getMetadata(){
		return metadata;
	}

	public void setReferences(String references){
		this.references = references;
	}

	public String getReferences(){
		return references;
	}

	public void setMedia(String media){
		this.media = media;
	}

	public String getMedia(){
		return media;
	}

	public void setTalkPageHtml(String talkPageHtml){
		this.talkPageHtml = talkPageHtml;
	}

	public String getTalkPageHtml(){
		return talkPageHtml;
	}
}