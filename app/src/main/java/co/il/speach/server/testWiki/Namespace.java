package co.il.speach.server.testWiki;

import com.google.gson.annotations.SerializedName;

public class Namespace{

	@SerializedName("id")
	private int id;

	@SerializedName("text")
	private String text;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}
}