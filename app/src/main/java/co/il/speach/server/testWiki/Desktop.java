package co.il.speach.server.testWiki;

import com.google.gson.annotations.SerializedName;

public class Desktop{

	@SerializedName("edit")
	private String edit;

	@SerializedName("revisions")
	private String revisions;

	@SerializedName("talk")
	private String talk;

	@SerializedName("page")
	private String page;

	public void setEdit(String edit){
		this.edit = edit;
	}

	public String getEdit(){
		return edit;
	}

	public void setRevisions(String revisions){
		this.revisions = revisions;
	}

	public String getRevisions(){
		return revisions;
	}

	public void setTalk(String talk){
		this.talk = talk;
	}

	public String getTalk(){
		return talk;
	}

	public void setPage(String page){
		this.page = page;
	}

	public String getPage(){
		return page;
	}
}