package co.il.speach.server.testWiki;

import com.google.gson.annotations.SerializedName;

public class Titles{

	@SerializedName("normalized")
	private String normalized;

	@SerializedName("display")
	private String display;

	@SerializedName("canonical")
	private String canonical;

	public void setNormalized(String normalized){
		this.normalized = normalized;
	}

	public String getNormalized(){
		return normalized;
	}

	public void setDisplay(String display){
		this.display = display;
	}

	public String getDisplay(){
		return display;
	}

	public void setCanonical(String canonical){
		this.canonical = canonical;
	}

	public String getCanonical(){
		return canonical;
	}
}