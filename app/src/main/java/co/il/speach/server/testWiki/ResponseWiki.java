package co.il.speach.server.testWiki;

import com.google.gson.annotations.SerializedName;

public class ResponseWiki{

	@SerializedName("wikibase_item")
	private String wikibaseItem;

	@SerializedName("thumbnail")
	private Thumbnail thumbnail;

	@SerializedName("api_urls")
	private ApiUrls apiUrls;

	@SerializedName("description")
	private String description;

	@SerializedName("extract_html")
	private String extractHtml;

	@SerializedName("titles")
	private Titles titles;

	@SerializedName("type")
	private String type;

	@SerializedName("title")
	private String title;

	@SerializedName("pageid")
	private int pageid;

	@SerializedName("dir")
	private String dir;

	@SerializedName("tid")
	private String tid;

	@SerializedName("revision")
	private String revision;

	@SerializedName("originalimage")
	private Originalimage originalimage;

	@SerializedName("extract")
	private String extract;

	@SerializedName("namespace")
	private Namespace namespace;

	@SerializedName("displaytitle")
	private String displaytitle;

	@SerializedName("lang")
	private String lang;

	@SerializedName("timestamp")
	private String timestamp;

	@SerializedName("content_urls")
	private ContentUrls contentUrls;

	public void setWikibaseItem(String wikibaseItem){
		this.wikibaseItem = wikibaseItem;
	}

	public String getWikibaseItem(){
		return wikibaseItem;
	}

	public void setThumbnail(Thumbnail thumbnail){
		this.thumbnail = thumbnail;
	}

	public Thumbnail getThumbnail(){
		return thumbnail;
	}

	public void setApiUrls(ApiUrls apiUrls){
		this.apiUrls = apiUrls;
	}

	public ApiUrls getApiUrls(){
		return apiUrls;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setExtractHtml(String extractHtml){
		this.extractHtml = extractHtml;
	}

	public String getExtractHtml(){
		return extractHtml;
	}

	public void setTitles(Titles titles){
		this.titles = titles;
	}

	public Titles getTitles(){
		return titles;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setPageid(int pageid){
		this.pageid = pageid;
	}

	public int getPageid(){
		return pageid;
	}

	public void setDir(String dir){
		this.dir = dir;
	}

	public String getDir(){
		return dir;
	}

	public void setTid(String tid){
		this.tid = tid;
	}

	public String getTid(){
		return tid;
	}

	public void setRevision(String revision){
		this.revision = revision;
	}

	public String getRevision(){
		return revision;
	}

	public void setOriginalimage(Originalimage originalimage){
		this.originalimage = originalimage;
	}

	public Originalimage getOriginalimage(){
		return originalimage;
	}

	public void setExtract(String extract){
		this.extract = extract;
	}

	public String getExtract(){
		return extract;
	}

	public void setNamespace(Namespace namespace){
		this.namespace = namespace;
	}

	public Namespace getNamespace(){
		return namespace;
	}

	public void setDisplaytitle(String displaytitle){
		this.displaytitle = displaytitle;
	}

	public String getDisplaytitle(){
		return displaytitle;
	}

	public void setLang(String lang){
		this.lang = lang;
	}

	public String getLang(){
		return lang;
	}

	public void setTimestamp(String timestamp){
		this.timestamp = timestamp;
	}

	public String getTimestamp(){
		return timestamp;
	}

	public void setContentUrls(ContentUrls contentUrls){
		this.contentUrls = contentUrls;
	}

	public ContentUrls getContentUrls(){
		return contentUrls;
	}
}