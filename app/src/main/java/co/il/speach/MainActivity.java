package co.il.speach;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igalata.bubblepicker.adapter.BubblePickerAdapter;
import com.igalata.bubblepicker.model.BubbleGradient;
import com.igalata.bubblepicker.model.PickerItem;
import com.igalata.bubblepicker.rendering.BubblePicker;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import co.il.speach.server.testWiki.ResponseWiki;
import io.cortical.retina.client.LiteClient;
import io.cortical.retina.rest.ApiException;

public class MainActivity extends AppCompatActivity implements BubbleAdapter.BubbleAdapterListener, ItemDialog.ItemDialogListener {

    private static final int MY_PERMISSIONS_RECORD_AUDIO = 101;
    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView mRecyclerRV;
    private SpeechRecognizer mSpeechRecognizer;
    private BubbleAdapter aBubbleAdapter;
    private Intent mSpeechRecognizerIntent;
    private List<String> keywords = new ArrayList<>();
    private LottieAnimationView mVoiceAnimationLAV;
    private boolean inSpeechMode;
    private BubblePicker mBubble_BP;
    private List<String> titles = new ArrayList<>();
    private TypedArray colors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
//        setSpeechRecognizer();
        setStartBtn();
    }


    private void initViews() {


//        mRecyclerRV = findViewById(R.id.MA_recycler_RV);
        mBubble_BP = findViewById(R.id.MA_picker_BP);
        mVoiceAnimationLAV = findViewById(R.id.MA_voice_animation_LAV);

    }


    @Override
    protected void onResume() {
        super.onResume();
        mBubble_BP.onResume();

//        setKeyWordsFromSpeech();



//        titles = Arrays.asList(getResources().getStringArray(R.array.countries));
        titles.add("a");
        titles.add("a");
        titles.add("a");
        titles.add("a");
        titles.add("a");
         colors = getResources().obtainTypedArray(R.array.colors);

        mBubble_BP.setAdapter(new BubblePickerAdapter() {
            @Override
            public int getTotalCount() {
                return titles.size();
            }

            @NotNull
            @Override
            public PickerItem getItem(int position) {
                PickerItem item = new PickerItem();
                item.setTitle(titles.get(position));
                item.setGradient(new BubbleGradient(colors.getColor((position * 2) % 8, 0),
                        colors.getColor((position * 2) % 8 + 1, 0), BubbleGradient.VERTICAL));
                item.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
                item.setTextColor(ContextCompat.getColor(MainActivity.this, android.R.color.white));
//                item.setBackgroundImage(ContextCompat.getDrawable(MainActivity.this, images.getResourceId(position, 0)));
                return item;
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        mBubble_BP.onPause();
    }


    private boolean checkRecordPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_RECORD_AUDIO);
            } else {

                return true;
            }

        }

        return false;
    }


    private void setSpeechRecognizer() {


        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
        mSpeechRecognizerIntent.putExtra("android.speech.extra.DICTATION_MODE", true);
//        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
//        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 20000L);

//        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 10000);


        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {
                Log.d(TAG, "SpeechRecognizer: " + "onReadyForSpeech: ");

            }

            @Override
            public void onBeginningOfSpeech() {
                Log.d(TAG, "SpeechRecognizer: " + "onBeginningOfSpeech: ");

                AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);

            }

            @Override
            public void onRmsChanged(float v) {
                Log.d(TAG, "SpeechRecognizer: " + "onRmsChanged: ");

            }

            @Override
            public void onBufferReceived(byte[] bytes) {
                Log.d(TAG, "SpeechRecognizer: " + "onBufferReceived: ");

            }

            @Override
            public void onEndOfSpeech() {

                Log.d(TAG, "SpeechRecognizer: " + "onEndOfSpeech: ");

            }

            @Override
            public void onError(int i) {
//                mTextTV.setHint("You will see input here");
//                Toast.makeText(MainActivity.this, "Speech Recognizer Error: " + i, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "SpeechRecognizer: " + "onError: " + i);

                setSpeechRecognizer();
                mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                mVoiceAnimationLAV.playAnimation();

            }

            @Override
            public void onResults(Bundle bundle) {


//                setKeyWordsFromSpeech();

                setSpeechRecognizer();
                mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                mVoiceAnimationLAV.playAnimation();


            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });


    }


    private void setKeyWordsFromSpeech() {


        //getting all the matches
//        final ArrayList<String> matches = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

        //displaying the first match
//        if (matches != null) {
//            mTextTV.setText(matches.get(0));
//        }


        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {

                    LiteClient lite = new io.cortical.retina.client.LiteClient("5cc7d460-e931-11e9-8f72-af685da1b20e");

                    try {
                                final List<String> keywords = lite.getKeywords("Vienna is the capital and largest city of Austria, and one of the nine states of Austria");
//                        List<String> liteKeywords = lite.getKeywords(Objects.requireNonNull(matches).get(0));
//                        keywords.addAll(liteKeywords);


                        if (keywords.size() <= 0) {

                            Toast.makeText(MainActivity.this, "Cant find Keywords", Toast.LENGTH_SHORT).show();

                        } else {

                            titles.addAll(keywords);
                            mBubble_BP.getAdapter().notify();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

//                                    titles.addAll(keywords);
//                                    mBubble_BP.getAdapter().notify();
//                                    final String[] titles = getResources().getStringArray(R.array.countries);
//                                    final TypedArray colors = getResources().obtainTypedArray(R.array.colors);
//                                    mBubble_BP.getAdapter().n

//                                    mBubble_BP.setAdapter(new BubblePickerAdapter() {
//                                        @Override
//                                        public int getTotalCount() {
//                                            return keywords.size();
//                                        }
//
//                                        @NotNull
//                                        @Override
//                                        public PickerItem getItem(int position) {
//                                            PickerItem item = new PickerItem();
//                                            item.setTitle(keywords.get(position));
//                                            item.setGradient(new BubbleGradient(colors.getColor((position * 2) % 8, 0),
//                                                    colors.getColor((position * 2) % 8 + 1, 0), BubbleGradient.VERTICAL));
//                                            item.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
//                                            item.setTextColor(ContextCompat.getColor(MainActivity.this, android.R.color.white));
////                item.setBackgroundImage(ContextCompat.getDrawable(MainActivity.this, images.getResourceId(position, 0)));
//                                            return item;
//                                        }
//                                    });

//                                    mRecyclerRV.setLayoutManager(new LinearLayoutManager(MainActivity.this));
//                                    aBubbleAdapter = new BubbleAdapter(MainActivity.this, keywords, MainActivity.this);
//                                    mRecyclerRV.setAdapter(aBubbleAdapter);
                                }
                            });

                        }


                    } catch (ApiException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();


    }


    private void setStartBtn() {

        mVoiceAnimationLAV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!inSpeechMode) {

                    if (checkRecordPermission()) {

//                        setKeyWordsFromSpeech();

//                        inSpeechMode = true;
//                        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(MainActivity.this);
//                        setSpeechRecognizer();
//                        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                        mVoiceAnimationLAV.playAnimation();

                         final List<String> titles2 = new ArrayList<>();



                        titles2.add("aaaaaaaaa");
                        titles2.add("aaaaaaaaa");
                        titles2.add("aaaaaaaaa");
                        titles2.add("aaaaaaaaa");
                        mBubble_BP.setVisibility(View.GONE);

                        mBubble_BP.setAdapter(new BubblePickerAdapter() {
                            @Override
                            public int getTotalCount() {
                                return titles2.size();
                            }

                            @NotNull
                            @Override
                            public PickerItem getItem(int position) {
                                PickerItem item = new PickerItem();
                                item.setTitle(titles2.get(position));
                                item.setGradient(new BubbleGradient(colors.getColor((position * 2) % 8, 0),
                                        colors.getColor((position * 2) % 8 + 1, 0), BubbleGradient.VERTICAL));
                                item.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
                                item.setTextColor(ContextCompat.getColor(MainActivity.this, android.R.color.white));
//                item.setBackgroundImage(ContextCompat.getDrawable(MainActivity.this, images.getResourceId(position, 0)));
                                return item;
                            }
                        });

                        mBubble_BP.setVisibility(View.VISIBLE);

                    }

                } else {


                    AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);

                    mSpeechRecognizer.destroy();
                    mVoiceAnimationLAV.cancelAnimation();
                    inSpeechMode  = false;

                }


            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == MY_PERMISSIONS_RECORD_AUDIO) {

            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);


        }
    }


    @Override
    public void onItemWikiFind(ResponseWiki results) {


        ItemDialog itemDialog = new ItemDialog(results);
        itemDialog.showDialog(this, this);

    }


    @Override
    public void onLinkWikiClicked(ResponseWiki itemDetailes) {

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(itemDetailes.getContentUrls().getMobile().getPage()));
        startActivity(i);


    }
}
